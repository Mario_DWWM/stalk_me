# Stalk_me

Stalk_me est une application qui permet de récolter les données de vos clients afin de leur proposer un service toujours plus ciblé. C'est une application *éthiquement au raz des paquerettes*, il faut le savoir !

**Instructions de démarrage de l'application**

> Lancement du serveur
```BASH
# Depuis la racine du projet :
cd Server/
# Si Nodemon est installé :
nodemon --ignore campains/
# Sinon :
node index.js
```

> Lancement du client
```BASH
# Depuis la racine du projet :
cd Client/
# Pour le développement :
npm run dev
# Pour la production :
npm run build
npm start
```