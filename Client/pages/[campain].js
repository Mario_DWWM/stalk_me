import Field from './../components/Field'
import { useState } from 'react'

export async function getStaticPaths() {
    let campains = await fetch('http://localhost:3001/campains').then(r => r.json())

    const paths = campains.map((campain) => ({
        params: { campain: campain }
    }))

    return {
        paths,
        fallback: false
    }
}

export async function getStaticProps({ params }) {
    let data = await fetch(`http://localhost:3001/campains/${params.campain}`).then(r => r.json())
    let name = params.campain

    return {
        props: {
            data,
            name
        }
    }
}

export default function HomePage({ data, name }) {
    const [form, setForm] = useState({})

    function updateForm(index, value) {
        let tempState = { ...form }
        tempState[index] = value
        setForm(tempState)
    }

    function sendForm(e, name, data) {
        e.preventDefault()

        fetch(`http://localhost:3001/${name}`, {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)

        }).then((res) => {

            setTimeout(() => submitMessage(res.ok), 600)

        }).catch((err) => {

            setTimeout(() => submitMessage(false), 600)

        })
    }

    function submitMessage(status) {
        let msg = document.createElement('p')
        msg.classList.add("submitMessage")

        if (status) {
            msg.innerText = "Votre participation a bien été prise en compte."
            msg.classList.add("validated")
        } else {
            msg.innerText = "Une erreur est survenue, veuillez réessayer ultérieurement."
            msg.classList.add("error")
        }

        document.body.appendChild(msg)

        setTimeout(() => document.body.removeChild(msg), 2800)
    }

    return (
        <>
            <h1>{data.title}</h1>
            <form onSubmit={(e) => sendForm(e, name, form)} className="column" >
                {data.contact.map((field, index) => {
                    if (field.state) {
                        return <Field label={field.name} type={field.name} key={index} update={updateForm} />
                    }
                })}
                {data.questions.map((field, index) => <Field label={field} type={false} key={index} update={updateForm} />)}
                <input type="submit" value="Envoyer" className="button" />
            </form>
        </>
    )
}