import CampainForm from "../components/CampainForm";
import { useState } from 'react'

export default function NewCampain() {
    const [form, setForm] = useState({})

    return <CampainForm form={form} update={setForm} />
}