import LoginForm from "../components/LoginForm"

export default function Login() {
    return (
        <>
            <h1>STALK<span className="highlight">ME</span></h1>
            <LoginForm />
        </>
    )
}