import styles from './AddField.module.scss'
import Image from 'next/image'

export default function AddField({ addQuestion }) {
    return (
        <div className={styles.container} onClick={addQuestion} >
            <Image src="/plus.svg" width={20} height={20} />
        </div>
    )
}