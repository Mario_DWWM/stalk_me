import styles from './Question.module.scss'
import Image from 'next/image'

export default function Question({ index, update, remove }) {
    return (
        <div className={styles.container} >
            <input
                type="text"
                placeholder="Tapez votre question ici"
                autoComplete="off"
                onChange={(e) => update(index, e.target.value)}
            />
            <div className={styles.closeButton} onClick={() => remove(index)} >
                <Image src="/plus.svg" width={20} height={20} />
            </div>
        </div>
    )
}