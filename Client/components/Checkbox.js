import styles from './Checkbox.module.scss'
import Image from 'next/image'

export default function Checkbox({ field, update }) {
    let style = field.state ? styles.checked : "";

    return (
        <div className={styles.contact} onClick={() => update(field.name)} >
            <div className={`${styles.checkBox} ${style}`} >
                {field.state && <Image src="/check.svg" width={10} height={10} layout={"responsive"} />}
            </div>
            <p>{field.name}</p>
        </div>
    )
}