import styles from './CampainForm.module.scss'
import Checkbox from './Checkbox'
import AddField from './AddField'
import Question from './Question'
import { useState } from 'react'

export default function CampainForm({ form, update }) {

    function sendForm(e) {
        e.preventDefault();

        let newForm = {
            title: title,
            contact: contact,
            questions: questions
        }

        update(newForm)

        fetch('http://localhost:3001/new', {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(newForm)

        }).then((res) => {

            if (res.ok) {
                return res.json().then((res) => {
                    setTimeout(() => submitMessage(res.url), 600)
                })
            }

        }).catch((err) => {
            console.log(err)
            setTimeout(() => submitMessage(false), 600)

        })
    }

    // TITLE
    const [title, setTitle] = useState("")

    // CONTACT
    const [contact, setContact] = useState([
        { name: "Nom", state: true },
        { name: "Prenom", state: true },
        { name: "Téléphone", state: true },
        { name: "Mail", state: true },
        { name: "Adresse", state: true },
    ])

    function updateContact(fieldToUpdate) {
        let tempState = [...contact]
        tempState.map((field) => {
            if (field.name === fieldToUpdate) field.state = !(field.state)
        })
        setContact(tempState)
    }

    // QUESTIONS
    const [questions, setQuestions] = useState([])

    function addQuestion() {
        let tempState = [...questions]
        tempState.push("")
        setQuestions(tempState)
    }

    function updateQuestion(index, value) {
        let tempState = [...questions]
        tempState[index] = value
        setQuestions(tempState)
    }

    function deleteQuestion(index) {
        let tempState = [...questions]
        tempState.splice(index, 1)
        setQuestions(tempState)
    }

    // SOUMISSION
    function submitMessage(res) {
        let msg = document.createElement('p')
        msg.classList.add("submitMessage")

        if (res) {
            msg.innerText = res
            msg.classList.add("validated")
        } else {
            msg.innerText = "Une erreur est survenue, veuillez réessayer ultérieurement."
            msg.classList.add("error")
        }

        document.body.appendChild(msg)

        setTimeout(() => document.body.removeChild(msg), 2800)
    }

    return (
        <form className={styles.container}>

            <input
                type="text"
                placeholder="Nom de la campagne"
                id="title"
                autoComplete="off"
                className={styles.title}
                onChange={(e) => setTitle(e.target.value)}
            />

            <h2>Coordonées</h2>

            {contact.map((field, index) => <Checkbox field={field} key={index} update={updateContact} />)}

            <h2>Questions</h2>

            {questions.map((field, index) => <Question field={field} index={index} key={index} update={updateQuestion} remove={deleteQuestion} />)}

            <AddField addQuestion={addQuestion} />

            <input type="submit" value="Valider et enregistrer" className="button" onClick={(e) => sendForm(e)} />

        </form>
    )
}