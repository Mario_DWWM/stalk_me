import styles from './LoginForm.module.scss'
import Image from 'next/image'
import { useState } from 'react'

export default function LoginForm() {
    const [username, setUsername] = useState("");
    const [usernameError, setUsernameError] = useState(false);
    const [password, setPassword] = useState("");
    const [passwordError, setPasswordError] = useState(false);
    const [loginError, setLoginError] = useState(false);

    function sendForm(e) {
        e.preventDefault();

        !username ? setUsernameError(true) : setUsernameError(false);
        !password ? setPasswordError(true) : setPasswordError(false);

        // TEST A CHANGER
        setLoginError(true);

        console.log(`USER : ${username} | PASSWORD : ${password}`)
    }

    let userStyle = usernameError ? styles.empty : "";
    let passStyle = passwordError ? styles.empty : "";
    let buttonStyle = loginError ? styles.error : "";

    return (
        <form className={styles.container}>

            <div className={`${styles.input} ${userStyle}`}>
                <div
                    className={styles.icon}
                    onClick={() => document.getElementById('email').focus()}
                >
                    <Image src="/user.svg" width={16} height={16} />
                </div>
                <input
                    type="text"
                    placeholder="Nom d'utilisateur"
                    id="email"
                    autoComplete="off"
                    onChange={(e) => setUsername(e.target.value)}
                />
            </div>

            <div className={`${styles.input} ${passStyle}`}>
                <div
                    className={styles.icon}
                    onClick={() => document.getElementById('password').focus()}
                >
                    <Image src="/password.svg" width={16} height={16} />
                </div>
                <input
                    type="password"
                    placeholder="Mot de passe"
                    id="password"
                    autoComplete="off"
                    onChange={(e) => setPassword(e.target.value)}
                />
            </div>

            <div className={buttonStyle}>
                <input type="submit" value="Se connecter" className="button" onClick={(e) => sendForm(e)} />
            </div>

        </form>
    )
}