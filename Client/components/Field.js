import styles from './Field.module.scss'

export default function Field({ label, type, update }) {

    // Ajout des '?' en cas d'oubli
    if (!type) {
        label = label.trim()
        label = label[label.length - 1] !== "?" ? label.concat(" ", "?") : label
    }

    // Définition du type d'input
    let typeOfInput;
    if (!type || type === "Nom" || type === "Prenom" || type === "Adresse") {
        typeOfInput = "text"
    } else if (type === "Mail") {
        typeOfInput = "email"
    } else if (type === "Téléphone") {
        typeOfInput = "tel"
    }

    return (
        <>
            <label htmlFor={label} className={styles.title}>{label} {!type ? "" : ":"}</label>
            <div className={styles.container}>
                <input type={typeOfInput} name={label} onChange={(e) => update(label, e.target.value)} required />
            </div>
        </>
    )
}