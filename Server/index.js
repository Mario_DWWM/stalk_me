const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs')
const cors = require('cors')
const app = express()
const port = 3001

app.use(cors())
app.use(bodyParser.json());

app.post('/new', (req, res) => {
    let fileName = req.body.title + "_" + Date.now()

    fs.mkdirSync(`./campains/${fileName}`)

    fs.writeFile(`./campains/${fileName}/schema.json`, JSON.stringify(req.body), err => {
        res.status(200)

        if (err) res.send(err)

        res.json({ url: fileName })
        res.send()
    })
})

app.post('/:campain', (req, res) => {
    let fileName = req.params.campain
    let timeStamp = Date.now()

    fs.writeFile(`./campains/${fileName}/${timeStamp}.json`, JSON.stringify(req.body), err => {
        res.status(200)

        if (err) res.send(err)

        res.send()
    })
})

app.get('/campains', (req, res) => {
    fs.readdir('./campains/', (err, folders) => {
        if (err) res.send(err)

        res.json(folders)
        res.send()
    })
})

app.get('/campains/:campain', (req, res) => {
    let rawdata = fs.readFileSync(`./campains/${req.params.campain}/schema.json`)
    let data = JSON.parse(rawdata)

    res.json(data)
    res.send()
})

app.listen(port, () => {
    console.log(`Server running at http://localhost:${port}`)
})